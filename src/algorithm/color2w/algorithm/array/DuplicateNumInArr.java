package algorithm.color2w.algorithm.array;

import java.util.HashMap;

/**
 * @author wjl <br/>
 * @version 1.0
 * @ClassName: DuplicateNumInArr <br/>
 * @Description:
 *
 * 在一个长度为 n 的数组里的所有数字都在 0 到 n-1 的范围内。数组中某些数字是重复的，但不知道有几个数字是重复的，
 * 也不知道每个数字重复几次。请找出数组中任意一个重复的数字。
 *
 * 1
 * 2
 * 3
 * 4
 * 5
 * Input:
 * {2, 3, 1, 0, 2, 5}
 *
 * Output:
 * 2
 *
 * 思路: 1.排序
 *       2.时间复杂度O(N) 空间复杂度O(N)
 *       HashMap key为数字，值为次数，
 *       每次遍历先取key，如果为空进行put操作
 *       否则返回当前数(该数值第二次出现)
 *
 *       3.时间复杂度O(N) 空间复杂度O(1)
 *         要求时间复杂度 O(N)，空间复杂度 O(1)。因此不能使用排序的方法，也不能使用额外的标记数组。
 *         对于这种数组元素在 [0, n-1] 范围内的问题，可以将值为 i 的元素调整到第 i 个位置上进行求解。
 *         以 (2, 3, 1, 0, 2, 5) 为例，遍历到位置 4 时，该位置上的数为 2，但是第 2 个位置上已经有一个 2 的值了，因此可以知道 2 重复：
 *
 * <br />
 * @date: 2019/12/31 15:08<br/>
 *
 */
public class DuplicateNumInArr {

    public static void main(String[] args) {
        int arr[] = {2, 3, 1, 0, 2, 5};
        System.out.println("-- "+repeatNum2(arr));
    }

    public static int repeatNum1(int arr[]){
        HashMap<Integer,String> map = new HashMap<>();
        for (int num : arr) {
            if(map.containsKey(num)){
                return num;
            }
            map.put(num,"1");
        }
        return 0;
    }

    public static int repeatNum2(int arr[]){
        if(arr == null || arr.length <= 0){
            return 0;
        }
        for(int i = 0; i < arr.length;i++){
            //判断当前遍历的数组元素和数组下标是否相同
            while(arr[i]!=i){
                //判断当前下标数组元素和该元素所在的数组下标位置的元素是否相同
                if (arr[arr[i]] == arr[i]){
                    return arr[i];
                }
                swap(i,arr[i],arr);
            }
        }
        return 0;
    }

    private static void swap(int num1, int num2, int[] arr) {
        int temp = arr[num1];
        arr[num1] = arr[num2];
        arr[num2] = temp;
    }


}
