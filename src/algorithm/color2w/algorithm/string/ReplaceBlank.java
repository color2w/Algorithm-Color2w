package algorithm.color2w.algorithm.string;

import java.util.HashMap;

/**
 * @author wjl <br/>
 * @version 1.0
 * @ClassName: ReplaceBlank <br/>
 * @Description: 0010 0000  040 32 0x20 (space) 空格
 * @date: 2020/1/2 8:40<br/>
 */
public class ReplaceBlank {
    public static void main(String[] args) {
        String param = "this is oppo!! ";
        System.out.println("-- "+replaceBlank2(new StringBuilder(param)));
    }

    //时间复杂度O(N) 空间复杂度O(N)
    private static String replaceBlank1(String param) {
        StringBuilder stringBuilder = new StringBuilder();
        for (char str: param.toCharArray()) {
            if (str==32){
                stringBuilder.append("%20");
            }else{
                stringBuilder.append(str);
            }
        }
        return stringBuilder.toString();

    }

    //时间复杂度O(N) 空间复杂度O(1)
    /*
        1.先遍历整个字符串，并让P1指向当前的末尾，如果有空格，将整个字符串长度加2
        2.将P2指向加长后的字符串，开始循环，条件 P1>=0或者P1<P2
                                        如果P1指向的字符是空格，从P2开始替换
                                        否则将P1的字符给P2
     */
    private static String replaceBlank2(StringBuilder str) {
        int P1 = str.length() - 1;
        for (int i = 0; i <= P1; i++) {
            if (str.charAt(i) == ' ') {
                str.append("  ");
            }
        }
        int P2 = str.length() - 1;
        while (P1 >= 0 && P2 > P1) {
            char c = str.charAt(P1--);
            if (c == ' ') {
                str.setCharAt(P2--, '0');
                str.setCharAt(P2--, '2');
                str.setCharAt(P2--, '%');
            } else {
                str.setCharAt(P2--, c);
            }
        }
        return str.toString();
    }

}
